#include <algorithm>
#include <queue>
#include <list>
#include "Graph_Prim.h"
using namespace std;

Graph::Graph(int& NVertex) :isReach(NVertex, 0), lastNode(NVertex)
{
	for (int i = 0; i < NVertex; i++) {
		list<AdjNode> NodeList;
		AdjList.push_back(NodeList);
	}
}

void Graph::AddNode(int& first, int& second, int& weight)
{
	AdjNode NewNode;
	NewNode.index = second;
	NewNode.weight = weight;
	AdjList[first].push_back(NewNode);

	NewNode.index = first;
	NewNode.weight = weight;
	AdjList[second].push_back(NewNode);
}

void Graph::MST(vector<Edge>& Answer)
{
	list<AdjNode>::iterator it;

	lastNode--;
	isReach[0] = true;
	for (it = AdjList[0].begin(); it != AdjList[0].end(); it++) {
		pq.push(Node(0, it->index, it->weight));
	}

	while (lastNode > 0) {
		Node now = pq.top();
		pq.pop();

		if (isReach[now.index] == true) {
			continue;
		}

		lastNode--;
		isReach[now.index] = true;
		Answer.push_back(Edge(min(now.from, now.index), max(now.from, now.index), now.weight));
		for (it = AdjList[now.index].begin(); it != AdjList[now.index].end(); it++) {
			if (isReach[it->index] == false) {
				pq.push(Node(now.index, it->index, it->weight));
			}
		}
	}
}
