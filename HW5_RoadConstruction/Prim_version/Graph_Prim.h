
#ifndef _Graph_Prim_H_
#define _Graph_Prim_H_

#include <iostream>
#include <fstream>
#include <cstring>
#include <vector>
#include <list>
#include <queue>
#include <stdlib.h>

using namespace std;

struct Edge {
	int FirstNode, SecondNode, weight;
	Edge() {}
	Edge(int n1, int n2, int w) :FirstNode(n1), SecondNode(n2), weight(w) {}
};

struct AdjNode {
	int index, weight;
	AdjNode() {}
	AdjNode(int a, int b) :index(a), weight(b) {}
};

class Node {
public:
	int from, index, weight;
	Node() {}
	Node(int a, int b, int c) :from(a), index(b), weight(c) {}
};

class Graph {
private:
	class comp
	{
	public:
		bool operator()(const Node& n1, const Node& n2)const { return n1.weight > n2.weight; }
	};

	int NumVertex, lastNode;
	priority_queue<Node, vector<Node>, comp> pq;
	vector< list<AdjNode> > AdjList;
	vector<bool> isReach;
public:
	void AddNode(int& first, int& second, int& weight);
	void MST(vector<Edge>& Answer);
	Graph(int& NVertex);
};

#endif
