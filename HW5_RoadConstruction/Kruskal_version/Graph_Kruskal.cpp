#include <queue>
#include <vector>
#include "Graph_Kruskal.h"
using namespace std;

int DisjointSet::findRoot(int n)
{
	return root[n] == -1 ? n : root[n] = findRoot(root[n]);
}

bool DisjointSet::combine(int n1, int n2)
{
	int r1 = findRoot(n1);
	int r2 = findRoot(n2);

	if (r1 != r2) {
		root[r1] = r2;
		TreeNum--;
		return true;
	}
	else {
		return false;
	}
}

Graph::Graph(int& NVertex) :ds(NVertex), NumVertex(NVertex)
{
}

void Graph::AddNode(int& first, int& second, int& weight)
{
	pq.push(Edge(first, second, weight));
}

void Graph::MST(vector<Edge>& Answer) 
{
	while (pq.size() != 0 && ds.lastTree() > 1) {
		if (ds.combine(pq.top().FirstNode, pq.top().SecondNode)) {
			Answer.push_back(pq.top());
		}
		pq.pop();
	}
}
