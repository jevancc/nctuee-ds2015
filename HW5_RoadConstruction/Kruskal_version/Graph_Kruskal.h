#ifndef _Graph_Kruskal_H_
#define _Graph_Kruskal_H_

#include <cstring>
#include <vector>
#include <queue>
using namespace std;

struct Edge {
	int FirstNode, SecondNode, weight;
	Edge() {}
	Edge(int n1, int n2, int w) :FirstNode(n1), SecondNode(n2), weight(w) {}
};

class DisjointSet
{
private:
	int NumVertex, TreeNum;
	vector<int> root;
public:
	DisjointSet(int NVertex) :NumVertex(NVertex), TreeNum(NVertex), root(NVertex + 10, -1) {}
	int findRoot(int n);
	int lastTree() {
		return TreeNum;
	}
	bool combine(int a, int b);
};

class Graph {
private:
	class comp
	{
	public:
		bool operator()(const Edge& e1, const Edge& e2)const { return e1.weight > e2.weight; }
	};
	int NumVertex;
	priority_queue<Edge, vector<Edge>, comp> pq;
	DisjointSet ds;
public:
	void AddNode(int& first, int& second, int& weight);
	void MST(vector<Edge>& Answer);
	Graph(int& NVertex);
};

#endif
