//
//  set.cpp
//  DS_hw3_Stack_and_queue 
//
//  Created by chang keng-wei on 10/21/15.
//  Copyright (c) 2015 chang keng-wei. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <cstring>
#include "solve_stack.h"
using namespace std;

int cnt[MAX_CANDIDATE], minEnd, maxEnd;
EndNode endArr[MAX_END];
vector<int> positiveNum, endNum;

void Stack::pop() {
	if (topIt == -1) {
		cout << "ERROR" << endl;
		exit(-1);
	}
	cnt[topIt] = 0;
	topIt--;
}

void Stack::push(int n, bool isZero) {
	topIt++;
	isNewPush[topIt] = true;
	fromZero[topIt] = isZero;
	sumArr[topIt] = n;
}


void init()
{
	positiveNum.clear();
	for (int i = 0; i < endNum.size(); i++) {
		endArr[endNum[i]].clear();
	}
	endNum.clear();
}

void solve::calculate(int target, int limit, int candidates_amount, vector<int> candidates, vector< vector<int> > &solutions)
{
	minEnd = target, maxEnd = target;
	endArr[target].push();
	endNum.push_back(target);
	if (limit >= 1) {
		for (int i = 0; candidates[i] < 0; i++) {
			endArr[target - candidates[i]].push(candidates[i]);
			endNum.push_back(target - candidates[i]);
			maxEnd = max(maxEnd, target - candidates[i]);
		}
	}
	if (limit >= 2) {
		for (int i = 0; candidates[i] < 0; i++) {
			for (int j = i; candidates[j] < 0; j++) {
				endArr[target - (candidates[i] + candidates[j])].push(candidates[i], candidates[j]);
				endNum.push_back(target - (candidates[i] + candidates[j]));
				maxEnd = max(maxEnd, target - (candidates[i] + candidates[j]));
			}
		}
	}

	for (int i = 0; i < candidates_amount; i++) {
		if (candidates[i] > 0) {
			positiveNum.push_back(candidates[i]);
		}
	}

	Stack st;
	memset(cnt, 0, sizeof(cnt));

	st.push(0, true);
	while (st.size() != 0) {

		if (st.isTopNew()) {
			if (!st.isTopFromZero() && endArr[st.topSum()].isend()) {
				vector<int> tmp;
				for (int i = 0; i < st.size(); i++) {
					for (int j = 0; j < cnt[i]; j++) {
						tmp.push_back(positiveNum[i]);
					}
				}
				for (int i = 0; i < endArr[st.topSum()].size(); i++) {
					solutions.push_back(tmp);
					if (endArr[st.topSum()][i].n1 != NIL) {
						solutions.rbegin()->push_back(endArr[st.topSum()][i].n1);
					}
					if (endArr[st.topSum()][i].n2 != NIL) {
						solutions.rbegin()->push_back(endArr[st.topSum()][i].n2);
					}
				}
			}

			if (st.topSum() > maxEnd || st.size() - 1 == positiveNum.size()) {
				st.pop();
				continue;
			}

			st.push(st.topSum(), true);
			continue;
		}

		if (st.topSum() <= maxEnd) {
			st.topSum() += positiveNum[st.size() - 1];
			cnt[st.size() - 1]++;
			st.push(st.topSum(), false);
			continue;
		}

		st.pop();
	}
	init();
}

