//
//  set.h
//  DS_hw3_Stack_and_queue 
//
//  Created by chang keng-wei on 10/21/15.
//  Copyright (c) 2015 chang keng-wei. All rights reserved.
//

#ifndef __DS_hw3_solve__
#define __DS_hw3_solve__

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <stdlib.h>
using namespace std;

#define NIL 0x3f3f3f3f
#define MAX_CANDIDATE 305
#define MAX_END 15000

class solve {
private:

public:
	void calculate(int, int, int, vector<int>, vector< vector<int> > &);
	solve() {}
};

class Pair
{
public:
	int n1, n2;
	Pair() {}
	Pair(int a, int b = NIL) :n1(a), n2(b) {}
};

class EndNode
{
public:
	bool is_end;
	vector<Pair> numPair;
	EndNode() :is_end(false) {}
	void push(int a = NIL, int b = NIL) {
		is_end = true;
		numPair.push_back(Pair(a, b));
	}
	void clear() {
		is_end = false;
		numPair.clear();
	}
	bool isend() { return is_end; }
	int size() {
		return numPair.size();
	}
	Pair& operator[](int i) {
		return numPair[i];
	}
};

class Stack
{
public:
	bool isNewPush[MAX_CANDIDATE], fromZero[MAX_CANDIDATE];
	int sumArr[MAX_CANDIDATE], topIt;

	Stack() :topIt(-1) {}

	bool isTopNew() {
		return isNewPush[topIt];
	}
	bool isTopFromZero() {
		return fromZero[topIt];
	}
	int& topSum() {
		isNewPush[topIt] = false;
		return sumArr[topIt];
	}
	int size() {
		return topIt + 1;
	}

	void push(int n, bool isZero);
	void pop();
};

#endif
