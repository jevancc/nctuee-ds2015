////
//  set.h
//  DS_hw3_Stack_and_queue 
//
//  Created by chang keng-wei on 10/21/15.
//  Copyright (c) 2015 chang keng-wei. All rights reserved.
//

#ifndef __DS_hw3_solve__
#define __DS_hw3_solve__

#include <iostream>
#include <fstream>
#include <string.h>
#include <vector>
#include <stdlib.h>
using namespace std;

#define NIL 0x3f3f3f3f
#define MAX_CANDIDATE_NUM 305

class solve {
private:

public:
    void calculate(int, int, int, vector<int>, vector< vector<int> > &);
    solve() {}
};

class Pair
{
public:
	int n1, n2;
	Pair(int a = NIL, int b = NIL) :n1(a), n2(b) {}
};

class EndNode
{
public:
	int n;
	vector<Pair> numPair;
	EndNode(int a = NIL, vector<Pair> vec = vector<Pair>()) :n(a), numPair(vec) {}
	void push(int a = NIL, int b = NIL) {
		numPair.push_back(Pair(a, b));
	}
	void clear() {
		numPair.clear();
	}
	int size() {
		return numPair.size();
	}
	Pair& operator[](int i) {
		return numPair[i];
	}
	bool operator<(const int num)const {
		return n < num;
	}
	bool operator>(const int num)const {
		return n > num;
	}
};

#endif
