//
//  set.cpp
//  DS_hw3_Stack_and_queue 
//
//  Created by chang keng-wei on 10/21/15.
//  Copyright (c) 2015 chang keng-wei. All rights reserved.
//

#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <stack>
#include "solve.h"
using namespace std;

int cnt[MAX_CANDIDATE_NUM], minEnd, maxEnd, lastNum;
vector<int> positiveNum, tmp;
vector<EndNode> endNum;

inline vector<EndNode>::iterator upBound(vector<EndNode>& vec, int num)
{
	vector<EndNode>::iterator reIt;
	for (reIt = vec.begin(); reIt->n < num&&reIt != vec.end(); reIt++);
	return reIt;
}

void dfs(int depth, int sum, bool fromZero, vector< vector<int> > &solutions)
{
	if (!fromZero) {
		vector<EndNode>::iterator it = upBound(endNum, sum);
		for (; it != endNum.end(); it++) {
			if ((it->n - sum) % lastNum != 0)continue;
			
			int lastCnt = (it->n - sum) / lastNum;
			for (int i = 0; i < depth; i++) {
				for (int j = 0; j < cnt[i]; j++) {
					tmp.push_back(positiveNum[i]);
				}
			}
			for (int i = 0; i < lastCnt; i++) {
				tmp.push_back(lastNum);
			}
			for (int i = 0; i < (int)it->numPair.size(); i++) {
				solutions.push_back(tmp);
				if (it->numPair[i].n1 != NIL) {
					solutions.rbegin()->push_back(it->numPair[i].n1);
				}
				if (it->numPair[i].n2 != NIL) {
					solutions.rbegin()->push_back(it->numPair[i].n2);
				}
			}
			tmp.clear();
		}
	}
	if (sum > maxEnd || depth == positiveNum.size()) {
		return;
	}

	bool isZero = true;
	do {
		dfs(depth + 1, sum, isZero, solutions);
		
		sum += positiveNum[depth];
		cnt[depth]++;
		isZero = false;
	} while (sum <= maxEnd);
	cnt[depth] = 0;
	
	return;
}

void init()
{
	tmp.reserve(128);
	memset(cnt, 0, sizeof(cnt));
	positiveNum.clear();
	endNum.clear();
}

void solve::calculate(int target, int limit, int candidates_amount, vector<int> candidates, vector< vector<int> > &solutions)
{
	map<int, vector<Pair> > endList;
	map<int, vector<Pair> >::iterator mapIt;

	init();
	sort(candidates.begin(), candidates.end());
	minEnd = target, maxEnd = target;
	endList[target].push_back(Pair());
	if (limit >= 1) {
		for (int i = 0; candidates[i] < 0; i++) {
			endList[target - candidates[i]].push_back(Pair(candidates[i]));
			maxEnd = max(maxEnd, target - candidates[i]);
		}
	}
	if (limit >= 2) {
		for (int i = 0; candidates[i] < 0; i++) {
			for (int j = i; candidates[j] < 0; j++) {
				endList[target - (candidates[i] + candidates[j])].push_back(Pair(candidates[i], candidates[j]));
				maxEnd = max(maxEnd, target - (candidates[i] + candidates[j]));
			}
		}
	}

	for (mapIt = endList.begin(); mapIt != endList.end(); mapIt++) {
		endNum.push_back(EndNode(mapIt->first, mapIt->second));
	}

	for (int i = 0; i < candidates_amount; i++) {
		if (candidates[i] > 0) {
			positiveNum.push_back(candidates[i]);
		}
	}
	reverse(positiveNum.begin(), positiveNum.end());
	
	lastNum = *positiveNum.rbegin();
	positiveNum.pop_back();
	
	dfs(0, 0, false, solutions);
}

