//
// Gcd.h
// DS hw2 array gcd
//

#ifndef _GCD_
#define _GCD_
#define ARRAY_MAX 1005
#include <algorithm>
using namespace std;

typedef long long ll;

class GCD {
private:
public:
	GCD() {}
	~GCD() {}
	long long* FindGCD(long long *in1, long long *in2);
};

#endif

