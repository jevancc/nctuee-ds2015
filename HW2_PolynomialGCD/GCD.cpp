//
// GCD.cpp
// ds_hw2_array_gcd
//
#include <iostream>
#include <algorithm>
#include <cstring>
#include "GCD.h"
using namespace std;

ll gcdll(ll a, ll b)
{
	return b == 0LL ? a : abs(gcdll(b, a%b));
}

int find_deg(ll* in)
{
	int re;
	for (re = ARRAY_MAX - 1; re > 0 && in[re] == 0; re--);
	return re;
}

void simplify_poly(ll* in, int deg = -1)
{
	if (deg == -1)deg = find_deg(in);
	if (deg == 0)return;

	ll gcdval = abs(in[deg]);
	for (int i = deg; i >= 0 && gcdval != 1; i--) {
		if (in[i] == 0)continue;
		gcdval = gcdll(gcdval, abs(in[i]));
	}

	if (gcdval != 1) {
		for (int i = deg; i >= 0; i--) {
			in[i] /= gcdval;
		}
	}
	if (in[deg] < 0) {
		for (int i = deg; i >= 0; i--) {
			if (in[i] == 0)continue;
			in[i] = -in[i];
		}
	}
}

ll* GCD::FindGCD(ll *in1, ll *in2) {
	int max2 = find_deg(in2), max1 = find_deg(in1);
	simplify_poly(in1, max1);
	simplify_poly(in2, max2);

	while (true) {
		if (max1 == 0 || max2 == 0) {
			memset(in1, 0, sizeof(ll)*ARRAY_MAX);
			in1[0] = 1;
			return in1;
		}

		if (max1 >= max2) {
			swap(in1, in2);
			swap(max1, max2);
		}

		ll  gcdval = gcdll(in1[max1], in2[max2]);
		ll  m1 = in2[max2] / gcdval, m2 = in1[max1] / gcdval;

		for (int i = 0; i <= max2; i++) {
			in2[i] = m2 * in2[i];
		}
		for (int i = max2, j = max1; j >= 0; i--, j--) {
			in2[i] -= in1[j] * m1;
		}
		max1 = find_deg(in1);
		max2 = find_deg(in2);
		simplify_poly(in2, max2);
		simplify_poly(in1, max1);

		if (in2[max2] == 0) {
			return in1;
		}
	}
}