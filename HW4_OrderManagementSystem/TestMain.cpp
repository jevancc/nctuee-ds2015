#include <iostream>
#include <algorithm>
#include <cstring>
#include <vector>
#include <ctime>
#include <cstdlib>
#include "OrderMGMT.h"
using namespace std;

int main()
{
	vector<int> vec;
	list<unsigned> ans;
	list<unsigned>::iterator it;
	OrderMGMT bst;

	for (int i = 1; i <= 100; i++) {
		//cout << "TEST INSERT::" << i << endl;
		bst.addOrder(i, i );
	}
	for (int i = 1; i < 100; i++) {
		if (!bst.find(i)) {
			cout << "NOT FOUND" << endl;
		}
	}

	//ans = bst.searchByDateOrdering(1, 5);
	//for (it = ans.begin(); it != ans.end(); it++) {
	//	cout << *it << " ";
	//}
	//cout << endl;

	for (int i = 0; i < 10; i++) {
		bst.deleteOrders(i*10+3, i*10+7);
	}
	for (int i = 1; i <= 110; i++) {
		//cout << "TEST INSERT::" << i << endl;
		bst.addOrder(i, i);
	}
	ans = bst.searchByDateOrdering(1, 200);
	for (it = ans.begin(); it != ans.end(); it++) {
		cout << *it << " ";
	}
	cout << endl;

	system("pause");
	return 0;
}