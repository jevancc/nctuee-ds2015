#include <iostream>
#include <algorithm>
#include <map>
#include <list>
#include "OrderMGMT_STL_TEST.h"
using namespace std;

typedef pair<unsigned, unsigned> puu;
typedef map<unsigned, unsigned>::iterator mpit;

void OrderMGMT::addOrder(unsigned date, unsigned id)
{
	if (BST.find(date) == BST.end()) {
		BST.insert(puu(date, id));
	}
}

void OrderMGMT::deleteOrders(unsigned start, unsigned end)
{
	BST.erase(BST.lower_bound(start), BST.upper_bound(end));
}

list<unsigned> OrderMGMT::searchByDate(unsigned start, unsigned end)
{
	list<unsigned> ans;
	mpit it;

	for (it = BST.lower_bound(start); it != BST.upper_bound(end); it++) {
		if (it->first > end) {
			break;
		}
		else if (it->first >= start) {
			ans.push_back(it->second);
		}
	}
	return ans;
}

list<unsigned> OrderMGMT::searchByDateOrdering(unsigned start, unsigned end)
{
	list<unsigned> ans;

	unsigned cnt = 1;
	for (mpit it = BST.begin(); it != BST.end(); it++, cnt++) {
		if (cnt > end) {
			break;
		}
		else if (cnt >= start) {
			ans.push_back(it->second);
		}
	}
	return ans;
}