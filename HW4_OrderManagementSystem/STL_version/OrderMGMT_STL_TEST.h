#ifndef ORDERMGMT_STL_TEST_H
#define ORDERMGMT_STL_TEST_H

#include <list>
#include <map>
using namespace std;

// Data structure consisting of Order ID and date.
struct Node {
	unsigned id;
	unsigned date;
	unsigned leftSize;
	Node *left, *right;
};

// Order Management System consolidating key operational processes.
class OrderMGMT {
private:
	map<unsigned, unsigned> BST;
public:
	OrderMGMT() {}

	void addOrder(unsigned date, unsigned id);
	void deleteOrders(unsigned start, unsigned end);

	list<unsigned> searchByDate(unsigned start, unsigned end);
	list<unsigned> searchByDateOrdering(unsigned start, unsigned end);
};


#endif