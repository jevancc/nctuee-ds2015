#ifndef ORDERMGMT_H
#define ORDERMGMT_H

#include <list>
#include <climits>
#include <cstdlib>
using namespace std;

#define NIL 0

// Data structure consisting of Order ID and date.
struct Node {
	unsigned id;
	unsigned date;
	unsigned leftSize;
	Node *left, *right;
};

class NodePair
{
public:
	Node* t1, *t2;
	NodePair() {}
	NodePair(Node* a, Node* b) :t1(a), t2(b) {}
};

// Order Management System consolidating key operational processes.
class OrderMGMT {
private:
	static const int TYPE_NORMAL = 1;
	static const int TYPE_DUMMY = 2;
	int avIt, avListSize, size;
	Node *root, *avList;

	Node* newNode(unsigned date, unsigned id, Node* lc = NIL, Node* rc = NIL);
	void leftRotate(Node* &n);
	void rightRotate(Node* &n);
	Node* merge(Node* t1, Node* t2, int t1Size);
	NodePair split(Node* &rt, unsigned n);

	void searchByDateOrderingSlave(Node* &n, unsigned s, unsigned t, unsigned cnt);
	void searchByDateSlave(Node* &n, unsigned s, unsigned t);
public:
	list<unsigned> ansBuffer;
	OrderMGMT();
	bool insert(Node* &n, unsigned date, unsigned id, int flag);
	Node* find(unsigned date);

	void addOrder(unsigned date, unsigned id);
	void deleteOrders(unsigned start, unsigned end);

	list<unsigned> searchByDate(unsigned start, unsigned end);
	list<unsigned> searchByDateOrdering(unsigned start, unsigned end);
};

#endif

