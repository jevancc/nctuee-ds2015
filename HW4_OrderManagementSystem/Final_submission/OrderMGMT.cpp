#include "OrderMGMT.h"
#include <iostream>
#include <ctime>
#include <cstdlib>
#include <list>
#include <algorithm>
using namespace std;

OrderMGMT::OrderMGMT() :avIt(0)
{
	avListSize = 10000;
	avList = new Node[avListSize];
	root = NIL;
	size = 0;
}

Node* OrderMGMT::newNode(unsigned date, unsigned id, Node* lc, Node* rc)
{
	if (avIt == avListSize) {
		avList = new Node[avListSize];
		avIt = 0;
	}

	avList[avIt].date = date;
	avList[avIt].id = id;
	avList[avIt].left = lc;
	avList[avIt].right = rc;
	avList[avIt].leftSize = 0;
	return &avList[avIt++];
}

void OrderMGMT::leftRotate(Node* &n)
{
	Node* b = n->right;
	n->right->leftSize = n->right->leftSize + n->leftSize + 1;

	n->right = b->left;
	b->left = n;
	n = b;
}

void OrderMGMT::rightRotate(Node* &n)
{
	Node* b = n->left;
	n->leftSize = n->leftSize - n->left->leftSize - 1;

	n->left = b->right;
	b->right = n;
	n = b;
}

bool OrderMGMT::insert(Node* &n, unsigned date, unsigned id, int flag)
{
	bool isInsert = true;
	if (n == NIL) {
		n = newNode(date, id);
	}
	else if (date == n->date) {
		isInsert = false;
	}
	else if (date < n->date) {
		isInsert = insert(n->left, date, id, flag);
		if (isInsert) {
			n->leftSize++;
		}
		if (flag == OrderMGMT::TYPE_DUMMY) {
			rightRotate(n);
		}
	}
	else {
		isInsert = insert(n->right, date, id, flag);
		if (flag == OrderMGMT::TYPE_DUMMY) {
			leftRotate(n);
		}
	}
	return isInsert;
}


Node* OrderMGMT::find(unsigned date)
{
	Node* it = root;
	while (it != NIL) {
		if (it->date == date) {
			return it;
		}
		it = date < it->date ? it->left : it->right;
	}
	return NIL;
}

// Function to insert a new order.
// date: Date of a order.
// id: Order ID.
void OrderMGMT::addOrder(unsigned date, unsigned id)
{
	insert(root, date, id, OrderMGMT::TYPE_NORMAL);
}


// Function to delete orders from a given range.
// start: Begin date.
// end: End date.
void OrderMGMT::deleteOrders(unsigned start, unsigned end)
{
	int t1Size;
	Node *t1, *t2, *it = root;
	NodePair np;

	np = split(root, start);
	t1 = np.t1;
	t1Size = root->leftSize;
	root = np.t2;

	np = split(root, end);
	t2 = np.t2;

	root = merge(t1, t2, t1Size);
}


NodePair OrderMGMT::split(Node* &rt, unsigned n)
{
	insert(rt, n, 0, OrderMGMT::TYPE_DUMMY);
	return NodePair(rt->left, rt->right);
}

Node* OrderMGMT::merge(Node* t1, Node* t2, int t1Size)
{
	if (t1 == NIL) return t2;
	if (t2 == NIL) return t1;

	if (t2->left == NIL) {
		t2->left = t1;
		t2->leftSize = t1Size;
		return t2;
	}

	Node *rt = newNode(0, 0, t1, t2), *it = t2->left, *pre = t2;
	while (it->left != NIL) {
		pre->leftSize = pre->leftSize - 1;
		pre = it;
		it = it->left;
	}
	pre->leftSize = pre->leftSize - 1;
	pre->left = it->right;

	rt->id = it->id;
	rt->date = it->date;
	rt->leftSize = t1Size;
	return rt;
}

// Function to output a list of order IDs from a given range of dates.
// start Begin date.
// end End date.
// id_list Order IDs stored in the STL list.
list<unsigned> OrderMGMT::searchByDate(unsigned start, unsigned end)
{
	ansBuffer.clear();
	searchByDateSlave(root, start, end);
	return ansBuffer;
}

void OrderMGMT::searchByDateSlave(Node* &n, unsigned s, unsigned t)
{
	if (n == NIL) {
		return;
	}

	if (n->date >= s) {
		searchByDateSlave(n->left, s, t);
	}
	if (n->date >= s&&n->date <= t) {
		ansBuffer.push_back(n->id);
	}
	if (n->date <= t) {
		searchByDateSlave(n->right, s, t);
	}
	return;
}


// Function to output a list of order IDs from a given range of sequences.
// start Begin order.
// end End order.
// id_list Order IDs stored in the STL list.
list<unsigned> OrderMGMT::searchByDateOrdering(unsigned start, unsigned end)
{
	ansBuffer.clear();
	searchByDateOrderingSlave(root, start, end, 0);
	return ansBuffer;
}

void OrderMGMT::searchByDateOrderingSlave(Node* &n, unsigned s, unsigned t, unsigned cnt)
{
	if (n == NIL) {
		return;
	}

	int order = n->leftSize + cnt + 1;
	if (order >= s) {
		searchByDateOrderingSlave(n->left, s, t, cnt);
	}
	if (order >= s&&order <= t) {
		ansBuffer.push_back(n->id);
	}
	if (order <= t) {
		searchByDateOrderingSlave(n->right, s, t, order);
	}
}