// 
// PrimeChecker.h
// DS hw1 warm up
//

#ifndef _PrimeChecker_
#define _PrimeChecker_

#include <cstdlib>
#include <ctime>

typedef unsigned long long int ull;

class PRIMECHECKER {
private:
public:
	bool is_prime(ull n);
	ull fake_mul(ull n, ull m, ull x);
	ull fast_pow(ull n, ull p, ull x);
	long PrimeChecker(long, long);
	PRIMECHECKER() {}
};

#endif

